from __future__ import division
import math
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers

nums = 10000


def f(x):
    return 0.2 + 0.4 * x * x + 0.3 * x * math.sin(15 * x) + 0.05 * math.cos(50 * x)


data_x = np.array([np.array(item / nums) for item in range(-nums, nums)])
value_y = np.array([np.array(f(item / nums)) for item in range(-nums, nums)])
mean_x = np.mean(data_x)
std_x = np.std(data_x)


def norm(x):
    return (x - mean_x) / std_x


normed_train_data = norm(data_x)


def build_model():
    model = keras.Sequential([
        layers.Dense(64, activation='relu', input_shape=[1]),
        layers.Dense(32, activation='relu'),
        layers.Dense(16, activation='relu'),
        layers.Dense(8, activation='relu'),
        layers.Dense(4, activation='relu'),
        layers.Dense(1)
    ])

    optimizer = tf.keras.optimizers.RMSprop(0.001)

    model.compile(loss='mse',
                  optimizer=optimizer,
                  metrics=['mse'])
    return model


model = build_model()
print(model.summary())


# 通过为每个完成的时期打印一个点来显示训练进度
class PrintDot(keras.callbacks.Callback):
    def on_epoch_end(self, epoch, logs):
        if epoch % 100 == 0: print('')
        print('.', end='')


EPOCHS = 1000

# patience 值用来检查改进 epochs 的数量
early_stop = keras.callbacks.EarlyStopping(monitor='val_loss', patience=10)

history = model.fit(normed_train_data, value_y, epochs=EPOCHS,
                    validation_split=0.2, verbose=0, callbacks=[early_stop, PrintDot()])


def plot_history(history):
    hist = pd.DataFrame(history.history)
    hist['epoch'] = history.epoch

    plt.figure()
    plt.xlabel('Epoch')
    plt.ylabel('loss')
    plt.plot(hist['epoch'], hist['loss'],
             label='Train loss')
    plt.plot(hist['epoch'], hist['val_loss'],
             label='Val loss')
    plt.legend()
    plt.savefig('hist_loss.png')

    plt.figure()
    plt.xlabel('Epoch')
    plt.ylabel('Mean Square Error')
    plt.plot(hist['epoch'], hist['mean_squared_error'],
             label='Train Error')
    plt.plot(hist['epoch'], hist['val_mean_squared_error'],
             label='Val Error')
    plt.legend()
    plt.savefig('hist_MSE.png')


plot_history(history)

plt.figure()
plt.subplot(311)
plt.plot(model.predict(normed_train_data),
         label='predict')
plt.legend()
plt.subplot(312)
plt.plot(value_y,
         label='truth')
plt.legend()
plt.subplot(313)

plt.plot(model.predict(normed_train_data),
         label='predict')
plt.plot(value_y,
         label='truth')
plt.legend()
plt.savefig('pre.vs.tru.png')

evaluate_res = model.evaluate(normed_train_data, value_y, verbose=2)
test_predictions = model.predict(normed_train_data).flatten()
plt.figure()
plt.scatter(value_y, test_predictions)
plt.xlabel('True Values')
plt.ylabel('Predictions')
plt.axis('equal')
plt.axis('square')
plt.xlim([0, plt.xlim()[1]])
plt.ylim([0, plt.ylim()[1]])
_ = plt.plot([-100, 100], [-100, 100])
plt.savefig('tru.pre.scatter.png')
plt.figure()
error = test_predictions - value_y

plt.hist(error, bins=25)
plt.xlabel("Prediction Error")
_ = plt.ylabel("Count")
plt.savefig('err.png')
# plt.show()
user_input = float(input('plz input a float x (the value between -1,1):'))
pre = model.predict(np.array([norm(np.array([user_input]))]))
res = f(user_input)
print("your input is %.2f , and the real res is %.2f , and the model pre is %.2f" % (user_input, res, pre))
