import math
import matplotlib.pyplot as plt
import numpy as np
import keras
from keras import Sequential
from keras.layers import Dense




def f(x):
    return 0.2 + 0.4 * x * x + 0.3 * x * math.sin(15 * x) + 0.05 * math.cos(50 * x)

nums = 10000
data_x = np.array([np.array(item / nums) for item in range(-nums, nums)])
value_y = np.array([np.array(f(item / nums)) for item in range(-nums, nums)])
mean_x = np.mean(data_x)
std_x = np.std(data_x)


def norm(x):
    return (x - mean_x) / std_x


normed_train_data = norm(data_x)


def build_model():
    model = Sequential()
    model.add(Dense(64, activation='relu', input_shape=[1]))
    model.add(Dense(32, activation='relu'))
    model.add(Dense(16, activation='relu'))
    model.add(Dense(8, activation='relu'))
    model.add(Dense(4, activation='relu'))
    model.add(Dense(1))

    optimizer = keras.optimizers.RMSprop(lr=0.0001)

    model.compile(loss='mse',
                  optimizer=optimizer,
                  metrics=['mse'])
    return model


model = build_model()
print(model.summary())




# patience 值用来检查改进 epochs 的数量
early_stop = keras.callbacks.EarlyStopping(monitor='val_loss', patience=10)

history = model.fit(normed_train_data, value_y, epochs=1000,
                    validation_split=0.2, verbose=1, callbacks=[early_stop,])


def plot_history(history):
    hist={}
    hist['epoch'] = history.epoch
    hist['loss']=history.history['loss']
    hist['val_loss'] = history.history['val_loss']
    hist['mean_squared_error'] = history.history['mse']
    hist['val_mean_squared_error'] = history.history['val_mse']
    plt.figure()
    plt.xlabel('Epoch')
    plt.ylabel('loss')
    plt.plot(hist['epoch'], hist['loss'],
             label='Train loss')
    plt.plot(hist['epoch'], hist['val_loss'],
             label='Val loss')
    plt.legend()
    plt.savefig('hist_loss.png')

    plt.figure()
    plt.xlabel('Epoch')
    plt.ylabel('Mean Square Error')
    plt.plot(hist['epoch'], hist['mean_squared_error'],
             label='Train Error')
    plt.plot(hist['epoch'], hist['val_mean_squared_error'],
             label='Val Error')
    plt.legend()
    plt.savefig('hist_MSE.png')


plot_history(history)

plt.figure()
plt.subplot(311)
plt.plot(model.predict(normed_train_data),
         label='predict')
plt.legend()
plt.subplot(312)
plt.plot(value_y,
         label='truth')
plt.legend()
plt.subplot(313)

plt.plot(model.predict(normed_train_data),
         label='predict')
plt.plot(value_y,
         label='truth')
plt.legend()
plt.savefig('pre.vs.tru.png')

evaluate_res = model.evaluate(normed_train_data, value_y, verbose=2)
test_predictions = model.predict(normed_train_data).flatten()
plt.figure()
plt.scatter(value_y, test_predictions)
plt.xlabel('True Values')
plt.ylabel('Predictions')
plt.axis('equal')
plt.axis('square')
plt.xlim([0, plt.xlim()[1]])
plt.ylim([0, plt.ylim()[1]])
_ = plt.plot([-100, 100], [-100, 100])
plt.savefig('tru.pre.scatter.png')
plt.figure()
error = test_predictions - value_y

plt.hist(error, bins=25)
plt.xlabel("Prediction Error")
_ = plt.ylabel("Count")
plt.savefig('err.png')
user_input = float(input('plz input a float x (the value between -1,1):'))
pre = model.predict(np.array([norm(np.array([user_input]))]))
res = f(user_input)
print("your input is %.2f , and the real res is %.2f , and the model pre is %.2f" % (user_input, res, pre))
